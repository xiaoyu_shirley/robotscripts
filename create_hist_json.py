import json

def create():
    dic = dict()
    dic["pegah"] = \
        {
            "MEET":
                [
                    {"who": "chao", "date": "today", "time": "5pm", "goal": "get familiar with freeling"},
                    {"who": "shirley", "date": "tomorrow", "time": "5pm", "goal": "get familiar with date"},
                ],
            "REMIND":
                [
                    {"date": "next Monday", "time": "11am", "goal": "schedule meetings"}
                ]
        }
    dic["shirley"] = \
        {
            "MEET":
                [
                    {"who": "pegah", "date": "tomorrow", "time": "5pm", "goal": "get familiar with date"},
                ],
            "REMIND":
                [
                    {"date": "next tuesday", "time": "3pm", "goal": "discuss about database"},
                    {"date": "next monday", "time": "3pm", "goal": "dental appointment"},
                ]
        }
    dic["chao"] = \
        {
            "MEET":
                [
                    {"who": "pegah", "date": "today", "time": "5pm", "goal": "get familiar with freeling"}
                ],
            "REMIND":
                [
                    {"date": "today", "time": "4pm", "goal": "meeting with Pegah"}
                ]
    }





    with open("/home/xiaoyu/Documents/robot/altumrobotnlp/build/bin/data/intent_hist_data.json", "w") as f:
        json.dump(dic, f)


if __name__ == "__main__":
    create()
