import json


def create():
    dic = {
        "0": {

        },
        "1":
            {
                "setting_file": "/bin/data/office_setting_data.json",
                "hist_file": "/bin/data/office_hist_data.json",
                "user_group":
                    {
                        "0": {
                            "intent_default": "0",
                            "intent_choice": []
                        },
                        "1": {
                            "intent_default": "4",
                            "intent_choice": ["2", "3", "10"]
                        },
                        "2": {
                            "intent_default": "5",
                            "intent_choice": []
                        },
                        "3": {
                            "intent_default": "6",
                            "intent_choice": ["7", "8", "9"]
                        }
                    }
        }
    }

    with open("/home/chao/scenario_data.json", "w") as f:
        json.dump(dic, f)


if __name__ == "__main__":
    create()
