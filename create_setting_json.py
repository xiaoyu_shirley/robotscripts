import json


def create():
    dic = {
        "0": {
            "key_phrases": ["end"]
        },
        "1": {
            "key_phrases": ["cancel"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["\nIs there anything else I can help you with? You can say End if "
                                      "you'd like to end the Conversation."]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text", "value": ["\nIs there anything else I can help you with? You can say End if "
                             "you'd like to end the Conversation."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ]
            }
        },
        "2": {
            "key_phrases": ["book a meeting", "have a meeting", "schedule a meeting", "meeting with", "book meeting", "schedule meeting", "meeting schedule"],
            "1": {
                "slots": ["5"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text", "value": ["Sure $USERNAME, who would you like to meet?","Sure $USERNAME, who are you here to meet?"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "10"},
                            {"id": "extra", "value": "PersonList"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text", "value": ["Sure $USERNAME, who would you like to meet?","Sure $USERNAME,  who are you here to meet?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ]
            },
            "2": {
                "slots": ["2"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text", "value": ["What date would you like to meet with $WHO?", "$USERNAME, may I know the date of the meeting?"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text", "value": ["What date would you like to meet with $WHO?","$USERNAME, may I know the date of the meeting?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ]
            },
            "3": {
                "slots": ["3"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text", "value": ["What time would you like to meet with $WHO? Please use 'at' in the beginning."]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["What time would you like to meet with $WHO?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ]
            },
            "4": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["OK $USERNAME, Please say Yes to confirm the meeting with $WHO on $DATE at $TIME. You can say No to modify."]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["OK $USERNAME, Please say Yes to confirm the meeting, or say No to modify."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "require_choice": "true"
            },
            "-1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["Thank you $USERNAME, I have set up the meeting for you.", "Thank you $USERNAME, you are all set."]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["Thank you $USERNAME, I have set up the meeting for you.","Thank you $USERNAME, you are all set."]}
                        ]
                    },
                    {
                        "name": "Schedule Meeting",
                        "type": "SM",
                        "action_slots": [
                            {"id": "who", "value": "$WHO"},
                            {"id": "where", "value": "$WHERE"},
                            {"id": "date", "value": "$DATE"},
                            {"id": "time", "value": "$TIME"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "-2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["No problem. Please tell me what you want to change by saying: Who, Date, or Time \n"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"},
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["No problem. Please tell me what you want to change by saying: Who, Date, or Time \n"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }

                ],
            },
            "tirgger_intents": ["3"]
        },
        "3": {
            "key_phrases": ["set a reminder", "set an alarm", "remind me", "set reminder", "set alarm", "make reminder", "make a reminder"],
            "3": {
                "slots": ["1"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["What would you like to be reminded? Please use 'to' at the beginning. For example, to take my pills."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["What would you like to be reminded? Please use 'to' at the beginning."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
            },
            "1": {
                "slots": ["2"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["$USERNAME, What date would you like to have the reminder?","$USERNAME, may I know the date for the reminder?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["$USERNAME, What date would you like to have the reminder?","$USERNAME, may I know the date for the reminder?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": ["3"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["What time would you like to be reminded? Please use 'at' in the beginning."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["What time would you like to be reminded?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
            },
            "4": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["OK $USERNAME, Please say Yes to confirm the reminder for $WHAT on $DATE at $TIME. You can say No to modify."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["OK $USERNAME, Please say Yes to confirm the reminder, or say No to modify."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "require_choice": "true"
            },
            "-1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Thank you $USERNAME, I have set up the reminder for you.", "All done, thank you."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Thank you $USERNAME, I have set up the reminder for you.","All done, thank you."]}
                        ]
                    },
                    {
                        "name": "Set Reminder",
                        "type": "SM",
                        "action_slots": [
                            {"id": "what", "value": "$WHAT"},
                            {"id": "date", "value": "$DATE"},
                            {"id": "time", "value": "$TIME"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "-2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["No problem. Please tell me what you want to change by saying: What, Date, or Time \n"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["No problem. Please tell me what you want to change by saying: What, Date, or Time\n"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
            },
            "tirgger_intents": []
        },
        "4": {
            "key_phrases": ["greet employee"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["Hey $USERNAME!", "Hi $USERNAME!"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "1"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["Hey $USERNAME, I hope you have a great day!","Hi $USERNAME, what can I do for you?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "5": {
            "key_phrases": ["vip"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Good morning $USERNAME! Let me take you to the meeting room.", "Good day $USERNAME! I'll take you to the meeting room."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Good morning $USERNAME! Let me take you to the meeting room.","Good day $USERNAME! I'll take you to the meeting room."]}
                        ]
                    },
                    {
                        "name": "Move Robot",
                        "type": "MR",
                        "action_slots": [
                                {"id": "destination", "value": "Meeting"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Dr. Lu will be with you shortly. You can connect to wifi by scanning this code."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "4"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Dr. Lu will be with you shortly. You can connect to wifi by scanning this code."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "3": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Would you like some refreshments in the meantime?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "6"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Would you like some refreshments in the meantime?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "require_choice": "true"
            },
            "-1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Refreshments, coming right up!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Refreshments, coming right up!"]}
                        ]
                    },
                    {
                        "name": "Call Robot",
                        "type": "CR",
                        "action_slots": [
                                {"id": "destination", "value": "Meeting"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "-2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Ok, see you later!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Ok, see you later!"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "6": {
            "key_phrases": ["greet unknown"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Hi! May I help you?","Good day! How may I help you?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "1"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Hi! May I help you?","Good day! How may I help you?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                    "value": ["I can help you with these things.", "What can I help you with?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "7"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["I can help you with these things.","What can I help you with?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "7": {
            "key_phrases": ["deliver a package", "package delivery"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Let me show you where you can leave the packages.", "I'll take you to the package area, follow me!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Let me show you where you can leave the packages.", "I'll take you to the package area, follow me!"]}
                        ]
                    },
                    {
                        "name": "Move Robot",
                        "type": "MR",
                        "action_slots": [
                                {"id": "destination", "value": "Package"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Do you need a signature?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "9"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Do you need a signature?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "require_choice": "true"
            },
            "-1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["I'll take you to the manager. Follow me!", "The manager can help on that. Follow me!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["I'll take you to the manager. Follow me!","The manager can help on that. Follow me!"]}
                        ]
                    },
                    {
                        "name": "Move Robot",
                        "type": "MR",
                        "action_slots": [
                                {"id": "destination", "value": "Ye"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"

            },
            "-2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["OK, see you later!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["OK, see you later!"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "8": {
            "key_phrases": ["talk to a manager", "talk to manager", "talk to the manager"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["I'll take you to the manager. Follow me!", "Sure, I'll take you there!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["I'll take you to the manager. Follow me!","Sure, I'll take you there!"]}
                        ]
                    },
                    {
                        "name": "Move Robot",
                        "type": "MR",
                        "action_slots": [
                                {"id": "destination", "value": "Ye"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["You can connect to wifi by scanning this code."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "5"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["You can connect to wifi by scanning this code."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "9": {
            "key_phrases": ["see someone"],
            "1": {
                "slots": ["5"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["Who are you here to see?","Who would you like to see?"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "10"},
                                {"id": "extra", "value": "Person List"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["Who are you here to see?","Who would you like to see?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "1"}
                        ]
                    }
                ],
            },
            "2": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text",
                                    "value": ["I'll take you to $WHO. Follow me!"]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "8"},
                            {"id": "extra", "value": "$WHO"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["I'll take you to $WHO. Follow me!"]}
                        ]
                    },
                    {
                        "name": "Move Robot",
                        "type": "MR",
                        "action_slots": [
                                {"id": "destination", "value": "$WHO"}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "2"}
                        ]
                    }
                ],
            },
            "3": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                                {"id": "text", "value": ["You can connect to wifi by scanning this code."]},
                                {"id": "home", "value": "true"},
                                {"id": "screen", "value": "5"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                                {"id": "text",
                                 "value": ["You can connect to wifi by scanning this code."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                                {"id": "action", "value": "3"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            },
            "trigger_intents": []
        },
        "10": {
            "key_phrases": ["what be" , "what do" , "what can" , "what should", "how be", "how do", "how can", "how should", "where be", "where do", "where can", "who be" , "who do" , "who can" , "who should", "why be" , "why do" , "why can" , "why should", "when be" , "when do" , "when can" , "when should", "where be" , "what do" , "what can" , "what should"],
            "1": {
                "slots": ["7"],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["$PREDEFINED_TEXT"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["$PREDEFINED_TEXT"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "predefined": "true",
                "end_of_intent": "true"
            }
        },
        "12": {
            "key_phrases": ["not understand"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["I'm sorry, I don't know how to help with that.","Sorry, I don't understand.", "I’m sorry, I didn’t catch that.", "Sorry, I’m afraid I don’t follow you."]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["I'm sorry, I don't know how to help with that.","Sorry, I don't understand.", "I’m sorry, I didn’t catch that", "Sorry, I’m afraid I don’t follow you."]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            }
        },
        "13": {
            "key_phrases": ["wrong scenario"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["Please select the correct scenario!!"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            }
        },
        "14": {
            "key_phrases": ["fallback"],
            "1": {
                "slots": [],
                "actions": [
                    {
                        "name": "Set UI",
                        "type": "SU",
                        "action_slots": [
                            {"id": "text",
                             "value": ["I couldn't finish the job. \nIs there anything else I can help you with?"]},
                            {"id": "home", "value": "true"},
                            {"id": "screen", "value": "3"}
                        ]
                    },
                    {
                        "name": "Speak",
                        "type": "SP",
                        "action_slots": [
                            {"id": "text",
                             "value": ["I couldn't finish the job. \nIs there anything else I can help you with?"]}
                        ]
                    },
                    {
                        "name": "Exit Action",
                        "type": "EA",
                        "action_slots": [
                            {"id": "action", "value": "1"}
                        ]
                    }
                ],
                "end_of_intent": "true"
            }
        }
    }

    with open("/home/pegah/office_setting_data.json", "w") as f:
        
        json.dump(dic, f)


if __name__ == "__main__":
    create()
