import ast
import json

def convert():
    data_path = "/home/xiaoyu/Documents/robot/libraries/rdf-to-json/output/part-00000"
    with open(data_path, "r") as inf:
        lines = inf.readlines()

    dic = dict()
    for line in lines:
        tmp_dic = ast.literal_eval(line)
        if "name_en" in tmp_dic.keys():
            main_key = tmp_dic["name_en"]
            dic[main_key] = dict()
            for key in tmp_dic.keys():
                if key == "name_en":
                    continue
                dic[main_key][key] = tmp_dic[key]

    with open("person_data.json", "w") as out:
        json.dump(dic, out)


if __name__ == "__main__":
    convert()