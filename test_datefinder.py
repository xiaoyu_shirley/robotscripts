import datefinder


def test():
    string_with_dates = "entries are due at 8 this morning"

    matches = datefinder.find_dates(string_with_dates)
    for match in matches:
        print(match)


if __name__ == "__main__":
    test()